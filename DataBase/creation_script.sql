
CREATE TABLE Readout
(
	TankNumber            INTEGER NOT NULL,
	SensorNumber          INTEGER NOT NULL,
	ReadoutValue          FLOAT NOT NULL,
	ReadingTime           TIMESTAMP NOT NULL
)
;

ALTER TABLE Readout
	ADD  PRIMARY KEY (TankNumber,SensorNumber,ReadingTime)
;



	CREATE TABLE Sensor
	(
		TankNumber            INTEGER NOT NULL,
		SensorNumber          INTEGER NOT NULL,
		UnitCoefficient       FLOAT ,
		LoactionOnTankScheme  BLOB ,
		RangeLowerBound       FLOAT ,
		RangeUpperBound       FLOAT ,		
		SensorTypeName        VARCHAR(500) NOT NULL,
		LocationDescription   VARCHAR(10000) ,
		SensorPrecision       VARCHAR(500),
		DeviceModel           VARCHAR(500)
	)
	;

	ALTER TABLE Sensor
		ADD  PRIMARY KEY (TankNumber,SensorNumber)
	;



CREATE TABLE SensorType
(
	SensorTypeName        VARCHAR(500)  NOT NULL,
	TypeDescription       VARCHAR(10000) ,
	Unit                  VARCHAR(500)
)
;

ALTER TABLE SensorType
	ADD  PRIMARY KEY (SensorTypeName)
;



CREATE TABLE Tank
(
	TankNumber            INTEGER NOT NULL,
	Capacity              FLOAT ,
	Photo                 BLOB ,
	LocationAtScheme      BLOB
)
;

ALTER TABLE Tank
	ADD  PRIMARY KEY (TankNumber)
;



ALTER TABLE Readout
	ADD FOREIGN KEY (TankNumber,SensorNumber) REFERENCES Sensor(TankNumber,SensorNumber)
;


ALTER TABLE Readout
	ADD FOREIGN KEY (TankNumber) REFERENCES Tank(TankNumber)
;



ALTER TABLE Sensor
	ADD FOREIGN KEY (TankNumber) REFERENCES Tank(TankNumber)
;


ALTER TABLE Sensor
	ADD FOREIGN KEY (SensorTypeName) REFERENCES SensorType(SensorTypeName)
;


