﻿namespace OilDepot
{
    partial class TableGraphEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControlRootTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonView = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxSensorInfo = new System.Windows.Forms.TextBox();
            this.checkedListBoxSensors = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownToSecond = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownToMinute = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownToHout = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFromSeconds = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFromMinute = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFromHour = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControlGraphTableTab = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonChartOk = new System.Windows.Forms.Button();
            this.buttonChartSave = new System.Windows.Forms.Button();
            this.buttonChartSaveImage = new System.Windows.Forms.Button();
            this.buttonChartSetup = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonTableOk = new System.Windows.Forms.Button();
            this.buttonTableSave = new System.Windows.Forms.Button();
            this.buttonTableExportExcel = new System.Windows.Forms.Button();
            this.buttonTableSetup = new System.Windows.Forms.Button();
            this.tabControlRootTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToHout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromHour)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabControlGraphTableTab.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlRootTab
            // 
            this.tabControlRootTab.Controls.Add(this.tabPage1);
            this.tabControlRootTab.Controls.Add(this.tabPage2);
            this.tabControlRootTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlRootTab.Location = new System.Drawing.Point(0, 0);
            this.tabControlRootTab.Name = "tabControlRootTab";
            this.tabControlRootTab.SelectedIndex = 0;
            this.tabControlRootTab.Size = new System.Drawing.Size(537, 432);
            this.tabControlRootTab.TabIndex = 0;
            this.tabControlRootTab.SelectedIndexChanged += new System.EventHandler(this.tabControlRootTab_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.buttonView);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(529, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Настройка параметров";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(8, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Отмена";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonView
            // 
            this.buttonView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonView.Location = new System.Drawing.Point(395, 377);
            this.buttonView.Name = "buttonView";
            this.buttonView.Size = new System.Drawing.Size(126, 23);
            this.buttonView.TabIndex = 3;
            this.buttonView.Text = "Просмотр >>";
            this.buttonView.UseVisualStyleBackColor = true;
            this.buttonView.Click += new System.EventHandler(this.buttonView_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.textBoxSensorInfo);
            this.groupBox3.Controls.Add(this.checkedListBoxSensors);
            this.groupBox3.Location = new System.Drawing.Point(8, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(513, 259);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Датчики";
            // 
            // textBoxSensorInfo
            // 
            this.textBoxSensorInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSensorInfo.Location = new System.Drawing.Point(289, 19);
            this.textBoxSensorInfo.Multiline = true;
            this.textBoxSensorInfo.Name = "textBoxSensorInfo";
            this.textBoxSensorInfo.ReadOnly = true;
            this.textBoxSensorInfo.Size = new System.Drawing.Size(218, 230);
            this.textBoxSensorInfo.TabIndex = 1;
            // 
            // checkedListBoxSensors
            // 
            this.checkedListBoxSensors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.checkedListBoxSensors.FormattingEnabled = true;
            this.checkedListBoxSensors.Location = new System.Drawing.Point(11, 19);
            this.checkedListBoxSensors.Name = "checkedListBoxSensors";
            this.checkedListBoxSensors.Size = new System.Drawing.Size(272, 229);
            this.checkedListBoxSensors.TabIndex = 0;
            this.checkedListBoxSensors.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxSensors_ItemCheck);
            this.checkedListBoxSensors.SelectedValueChanged += new System.EventHandler(this.checkedListBoxSensors_SelectedValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numericUpDownToSecond);
            this.groupBox1.Controls.Add(this.numericUpDownToMinute);
            this.groupBox1.Controls.Add(this.numericUpDownToHout);
            this.groupBox1.Controls.Add(this.numericUpDownFromSeconds);
            this.groupBox1.Controls.Add(this.numericUpDownFromMinute);
            this.groupBox1.Controls.Add(this.numericUpDownFromHour);
            this.groupBox1.Controls.Add(this.dateTimePickerTo);
            this.groupBox1.Controls.Add(this.dateTimePickerFrom);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Интервал времени";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(373, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(374, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = ":";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(313, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(313, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = ":";
            // 
            // numericUpDownToSecond
            // 
            this.numericUpDownToSecond.Location = new System.Drawing.Point(387, 44);
            this.numericUpDownToSecond.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownToSecond.Name = "numericUpDownToSecond";
            this.numericUpDownToSecond.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownToSecond.TabIndex = 9;
            this.numericUpDownToSecond.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // numericUpDownToMinute
            // 
            this.numericUpDownToMinute.Location = new System.Drawing.Point(326, 44);
            this.numericUpDownToMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownToMinute.Name = "numericUpDownToMinute";
            this.numericUpDownToMinute.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownToMinute.TabIndex = 8;
            this.numericUpDownToMinute.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // numericUpDownToHout
            // 
            this.numericUpDownToHout.Location = new System.Drawing.Point(264, 44);
            this.numericUpDownToHout.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numericUpDownToHout.Name = "numericUpDownToHout";
            this.numericUpDownToHout.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownToHout.TabIndex = 7;
            this.numericUpDownToHout.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // numericUpDownFromSeconds
            // 
            this.numericUpDownFromSeconds.Location = new System.Drawing.Point(387, 17);
            this.numericUpDownFromSeconds.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownFromSeconds.Name = "numericUpDownFromSeconds";
            this.numericUpDownFromSeconds.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownFromSeconds.TabIndex = 6;
            this.numericUpDownFromSeconds.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // numericUpDownFromMinute
            // 
            this.numericUpDownFromMinute.Location = new System.Drawing.Point(326, 17);
            this.numericUpDownFromMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericUpDownFromMinute.Name = "numericUpDownFromMinute";
            this.numericUpDownFromMinute.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownFromMinute.TabIndex = 5;
            this.numericUpDownFromMinute.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // numericUpDownFromHour
            // 
            this.numericUpDownFromHour.Location = new System.Drawing.Point(264, 17);
            this.numericUpDownFromHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numericUpDownFromHour.Name = "numericUpDownFromHour";
            this.numericUpDownFromHour.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownFromHour.TabIndex = 4;
            this.numericUpDownFromHour.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Location = new System.Drawing.Point(48, 44);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerTo.TabIndex = 3;
            this.dateTimePickerTo.ValueChanged += new System.EventHandler(this.someValueChanged);
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Location = new System.Drawing.Point(48, 17);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerFrom.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "По";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "C";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControlGraphTableTab);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(529, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Просмотр";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControlGraphTableTab
            // 
            this.tabControlGraphTableTab.Controls.Add(this.tabPage3);
            this.tabControlGraphTableTab.Controls.Add(this.tabPage4);
            this.tabControlGraphTableTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGraphTableTab.Location = new System.Drawing.Point(3, 3);
            this.tabControlGraphTableTab.Name = "tabControlGraphTableTab";
            this.tabControlGraphTableTab.SelectedIndex = 0;
            this.tabControlGraphTableTab.Size = new System.Drawing.Size(523, 400);
            this.tabControlGraphTableTab.TabIndex = 0;
            this.tabControlGraphTableTab.SelectedIndexChanged += new System.EventHandler(this.tabControlGraphTableTab_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonChartOk);
            this.tabPage3.Controls.Add(this.buttonChartSave);
            this.tabPage3.Controls.Add(this.buttonChartSaveImage);
            this.tabPage3.Controls.Add(this.buttonChartSetup);
            this.tabPage3.Controls.Add(this.chart1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(515, 374);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "График";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonChartOk
            // 
            this.buttonChartOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChartOk.Location = new System.Drawing.Point(437, 350);
            this.buttonChartOk.Name = "buttonChartOk";
            this.buttonChartOk.Size = new System.Drawing.Size(75, 23);
            this.buttonChartOk.TabIndex = 4;
            this.buttonChartOk.Text = "ОК";
            this.buttonChartOk.UseVisualStyleBackColor = true;
            this.buttonChartOk.Click += new System.EventHandler(this.buttonChartOk_Click);
            // 
            // buttonChartSave
            // 
            this.buttonChartSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonChartSave.Location = new System.Drawing.Point(276, 350);
            this.buttonChartSave.Name = "buttonChartSave";
            this.buttonChartSave.Size = new System.Drawing.Size(144, 23);
            this.buttonChartSave.TabIndex = 3;
            this.buttonChartSave.Text = "Сохранить *.grh файл";
            this.buttonChartSave.UseVisualStyleBackColor = true;
            this.buttonChartSave.Click += new System.EventHandler(this.buttonChartSave_Click);
            // 
            // buttonChartSaveImage
            // 
            this.buttonChartSaveImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonChartSaveImage.Location = new System.Drawing.Point(125, 350);
            this.buttonChartSaveImage.Name = "buttonChartSaveImage";
            this.buttonChartSaveImage.Size = new System.Drawing.Size(145, 23);
            this.buttonChartSaveImage.TabIndex = 2;
            this.buttonChartSaveImage.Text = "Сохранить изображение";
            this.buttonChartSaveImage.UseVisualStyleBackColor = true;
            this.buttonChartSaveImage.Click += new System.EventHandler(this.buttonChartSaveImage_Click);
            // 
            // buttonChartSetup
            // 
            this.buttonChartSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonChartSetup.Location = new System.Drawing.Point(0, 351);
            this.buttonChartSetup.Name = "buttonChartSetup";
            this.buttonChartSetup.Size = new System.Drawing.Size(104, 23);
            this.buttonChartSetup.TabIndex = 1;
            this.buttonChartSetup.Text = "<< Настроить";
            this.buttonChartSetup.UseVisualStyleBackColor = true;
            this.buttonChartSetup.Click += new System.EventHandler(this.buttonChartSetup_Click);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(514, 335);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView1);
            this.tabPage4.Controls.Add(this.buttonTableOk);
            this.tabPage4.Controls.Add(this.buttonTableSave);
            this.tabPage4.Controls.Add(this.buttonTableExportExcel);
            this.tabPage4.Controls.Add(this.buttonTableSetup);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(515, 374);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Таблица";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(8, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(502, 327);
            this.dataGridView1.TabIndex = 10;
            // 
            // buttonTableOk
            // 
            this.buttonTableOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTableOk.Location = new System.Drawing.Point(439, 350);
            this.buttonTableOk.Name = "buttonTableOk";
            this.buttonTableOk.Size = new System.Drawing.Size(73, 23);
            this.buttonTableOk.TabIndex = 9;
            this.buttonTableOk.Text = "ОК";
            this.buttonTableOk.UseVisualStyleBackColor = true;
            this.buttonTableOk.Click += new System.EventHandler(this.buttonTableOk_Click);
            // 
            // buttonTableSave
            // 
            this.buttonTableSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTableSave.Location = new System.Drawing.Point(270, 349);
            this.buttonTableSave.Name = "buttonTableSave";
            this.buttonTableSave.Size = new System.Drawing.Size(144, 23);
            this.buttonTableSave.TabIndex = 8;
            this.buttonTableSave.Text = "Сохранить *.tbl файл";
            this.buttonTableSave.UseVisualStyleBackColor = true;
            this.buttonTableSave.Click += new System.EventHandler(this.buttonTableSave_Click);
            // 
            // buttonTableExportExcel
            // 
            this.buttonTableExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTableExportExcel.Location = new System.Drawing.Point(133, 349);
            this.buttonTableExportExcel.Name = "buttonTableExportExcel";
            this.buttonTableExportExcel.Size = new System.Drawing.Size(131, 23);
            this.buttonTableExportExcel.TabIndex = 7;
            this.buttonTableExportExcel.Text = "Экспорт в Excel";
            this.buttonTableExportExcel.UseVisualStyleBackColor = true;
            this.buttonTableExportExcel.Click += new System.EventHandler(this.buttonTableExportExcel_Click);
            // 
            // buttonTableSetup
            // 
            this.buttonTableSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTableSetup.Location = new System.Drawing.Point(-1, 350);
            this.buttonTableSetup.Name = "buttonTableSetup";
            this.buttonTableSetup.Size = new System.Drawing.Size(105, 23);
            this.buttonTableSetup.TabIndex = 6;
            this.buttonTableSetup.Text = "<< Настроить";
            this.buttonTableSetup.UseVisualStyleBackColor = true;
            this.buttonTableSetup.Click += new System.EventHandler(this.buttonTableSetup_Click);
            // 
            // TableGraphEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(537, 432);
            this.Controls.Add(this.tabControlRootTab);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(545, 465);
            this.Name = "TableGraphEdit";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Редактирование таблицы/графика";
            this.Load += new System.EventHandler(this.TableGraphEdit_Load);
            this.tabControlRootTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownToHout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFromHour)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabControlGraphTableTab.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlRootTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownToSecond;
        private System.Windows.Forms.NumericUpDown numericUpDownToMinute;
        private System.Windows.Forms.NumericUpDown numericUpDownToHout;
        private System.Windows.Forms.NumericUpDown numericUpDownFromSeconds;
        private System.Windows.Forms.NumericUpDown numericUpDownFromMinute;
        private System.Windows.Forms.NumericUpDown numericUpDownFromHour;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox checkedListBoxSensors;
        private System.Windows.Forms.Button buttonView;
        private System.Windows.Forms.TextBox textBoxSensorInfo;
        private System.Windows.Forms.TabControl tabControlGraphTableTab;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button buttonChartSave;
        private System.Windows.Forms.Button buttonChartSaveImage;
        private System.Windows.Forms.Button buttonChartSetup;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button buttonChartOk;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonTableOk;
        private System.Windows.Forms.Button buttonTableSave;
        private System.Windows.Forms.Button buttonTableExportExcel;
        private System.Windows.Forms.Button buttonTableSetup;
        private System.Windows.Forms.Button button1;
    }
}