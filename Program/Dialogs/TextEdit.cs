﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OilDepot
{
    public partial class TextEdit : Form
    {
        TextReportItem mData;

        public TextReportItem getReportItem() { return mData; }

        public TextEdit()
            : this(new TextReportItem())
        {
        }

        public TextEdit(TextReportItem item)
        {
            InitializeComponent();
            setData(item);
        }

        private void setData(TextReportItem item)
        {
            mData = item;
            textBox1.Text = item.mText;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            mData.mText = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void TextEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                
            }
        }
    }
}
