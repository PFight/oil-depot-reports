﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace OilDepot
{
    public partial class TableGraphEdit : Form
    {
        TableGraphSettings mSettings = null;
        TableGraphData mData = null;
        bool mProgramChange = false;

        public TableGraphData getData() { return mData; }
        public TableGraphSettings getSettings() { return mSettings; }

        public TableGraphEdit()
        {
            InitializeComponent();
            TableGraphSettings set = new TableGraphSettings();
            using (var con = DataBaseManager.OpenConnection())
            {
                foreach (SensorInfo s in TableGraphSettings.LoadSensorsInfo(con))
                {
                    set.mSensors.Add(s);
                }
            }
            setSettings(set);
        }

        public TableGraphEdit(TableGraphSettings initSettings): this()
        {            
            setSettings(initSettings);
            tabControlRootTab.SelectedIndex = 1;
            tabControlRootTab_SelectedIndexChanged(null, null);
        }

        private void setSettings(TableGraphSettings initSettings)
        {
            mSettings = initSettings;

            mProgramChange = true;

            dateTimePickerFrom.Value = mSettings.mTimeIntervalFrom;
            numericUpDownFromHour.Value = mSettings.mTimeIntervalFrom.Hour;
            numericUpDownFromMinute.Value = mSettings.mTimeIntervalFrom.Minute;
            numericUpDownFromSeconds.Value = mSettings.mTimeIntervalFrom.Second;

            dateTimePickerTo.Value = mSettings.mTimeIntervalTo;
            numericUpDownToHout.Value = mSettings.mTimeIntervalTo.Hour;
            numericUpDownToMinute.Value = mSettings.mTimeIntervalTo.Minute;
            numericUpDownToSecond.Value = mSettings.mTimeIntervalTo.Second;

            //checkBoxGroupEnable.Checked = mSettings.mGroupEnabled;

            //numericUpDownGroupDays.Value = mSettings.mGroupTimeStep.Days;
            //numericUpDownGroupHours.Value = mSettings.mGroupTimeStep.Hours;
            //numericUpDownGroupMinutes.Value = mSettings.mGroupTimeStep.Minutes;

            if (checkedListBoxSensors.Items.Count == 0)
            {
                using (var con = DataBaseManager.OpenConnection())
                {
                    foreach (SensorInfo s in TableGraphSettings.LoadSensorsInfo(con))
                    {
                        checkedListBoxSensors.Items.Add(s, mSettings.mSensors.Contains(s));
                    }
                }
            }
            else
            {
                foreach (SensorInfo sensor in mSettings.mSensors)
                {
                    checkedListBoxSensors.Items.Remove(sensor);
                    checkedListBoxSensors.Items.Add(sensor, true);
                }
            }
            tabControlGraphTableTab.SelectedIndex =  (mSettings.mType == ItemType.Graph)? 0 : 1;

            mProgramChange = false;
        }

        private void TableGraphEdit_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            tabControlRootTab.SelectedIndex++;
        }

        private void checkBoxGroupEnable_CheckedChanged(object sender, EventArgs e)
        {
            //numericUpDownGroupDays.Enabled = checkBoxGroupEnable.Checked;
            //numericUpDownGroupHours.Enabled = checkBoxGroupEnable.Checked;
            //numericUpDownGroupMinutes.Enabled = checkBoxGroupEnable.Checked;
            //comboBoxGroupAction.Enabled = checkBoxGroupEnable.Checked;
            someValueChanged(sender, e);
        }

        private void buttonChartSetup_Click(object sender, EventArgs e)
        {
            tabControlRootTab.SelectedIndex--;
        }

        private void buttonTableSetup_Click(object sender, EventArgs e)
        {
            tabControlRootTab.SelectedIndex--;
        }

        private void buttonTableOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void buttonChartOk_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void checkedListBoxSensors_SelectedValueChanged(object sender, EventArgs e)
        {
            SensorInfo sensor = checkedListBoxSensors.SelectedItem as SensorInfo;
            textBoxSensorInfo.Text = sensor.ToString() + Environment.NewLine +
                "Нижняя граница измерений: " + sensor.mRangeLowerBound + Environment.NewLine +
                "Верхняя граница измерений: " + sensor.mRangeUpperBound + Environment.NewLine +
                "Точность: " + sensor.mSensorPrecision + Environment.NewLine +
                "Модель датчика: " + sensor.mDeviceModel + Environment.NewLine +
                "Расположение в резервуаре: " + sensor.mLocationDescription;
            someValueChanged(sender, e);
        }

        private void checkedListBoxSensors_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            someValueChanged(sender, e);
        }

        private void someValueChanged(object sender, EventArgs e)
        {
            if (!mProgramChange)
            {
                DateTime dateFrom = dateTimePickerFrom.Value;
                mSettings.mTimeIntervalFrom = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day,
                    (int)numericUpDownFromHour.Value, (int)numericUpDownFromMinute.Value, (int)numericUpDownFromSeconds.Value);
                DateTime dateTo = dateTimePickerTo.Value;
                mSettings.mTimeIntervalTo = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day,
                    (int)numericUpDownToHout.Value, (int)numericUpDownToMinute.Value, (int)numericUpDownToSecond.Value);
                //mSettings.mGroupEnabled = checkBoxGroupEnable.Checked;
                //mSettings.mGroupTimeStep = new TimeSpan((int)numericUpDownGroupDays.Value,
                //    (int)numericUpDownGroupHours.Value, (int)numericUpDownGroupMinutes.Value, 0);
                //mSettings.mGroupAction = (GroupActions)comboBoxGroupAction.SelectedIndex;
                mSettings.mType = (tabControlGraphTableTab.SelectedIndex == 0) ? ItemType.Graph : ItemType.Table;
                mSettings.mSensors.Clear();
                foreach (SensorInfo sensor in checkedListBoxSensors.CheckedItems)
                {
                    mSettings.mSensors.Add(sensor);
                }
            }
        }

        private void tabControlGraphTableTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            someValueChanged(sender, e);
        }

        private void tabControlRootTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            someValueChanged(sender, e);

            if (tabControlRootTab.SelectedIndex == 1)
            {
                using (var con = DataBaseManager.OpenConnection())
                {
                    mData = TableGraphData.loadFromDataBase(con, mSettings);
                    mData.initChart(chart1);
                    mData.initTable(dataGridView1);
                }
            }
            else
            {
                mData = null;
            }
        }

        private void buttonChartSaveImage_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "Png image |*.png|AllFiles|*.*";
            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                chart1.SaveImage(sf.FileName, ChartImageFormat.Png);
            }
        }

        private void buttonTableExportExcel_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
            ExcelWorkBook = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
            ExcelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(1);
            for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                ExcelWorkSheet.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        ExcelWorkSheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
            

            ExcelApp.Visible = true;               
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private void buttonChartSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Graph files (*.grh)|*.grh|All files (*.*)|*.*";
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BinaryFormatter ser = new BinaryFormatter();
                this.Cursor = Cursors.WaitCursor;
                using(var stream = new FileStream(saveFile.FileName, FileMode.Create))
                {
                    ser.Serialize(stream, mData);
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void buttonTableSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Table files (*.tbl)|*.tbl|All files (*.*)|*.*";
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BinaryFormatter ser = new BinaryFormatter();
                this.Cursor = Cursors.WaitCursor;
                using (var stream = new FileStream(saveFile.FileName, FileMode.Create))
                {
                    ser.Serialize(stream, mData);
                }
                this.Cursor = Cursors.Default;
            }
        }
    }
}
