﻿namespace OilDepot
{
    partial class NewReportItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonExistingTable = new System.Windows.Forms.RadioButton();
            this.radioButtonExistingGraph = new System.Windows.Forms.RadioButton();
            this.radioButtonNewTableGraph = new System.Windows.Forms.RadioButton();
            this.radioButtonCaption = new System.Windows.Forms.RadioButton();
            this.textBoxTable = new System.Windows.Forms.TextBox();
            this.buttonOpenTable = new System.Windows.Forms.Button();
            this.buttonOpenGraph = new System.Windows.Forms.Button();
            this.textBoxGraph = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.radioButtonText = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // radioButtonExistingTable
            // 
            this.radioButtonExistingTable.AutoSize = true;
            this.radioButtonExistingTable.Location = new System.Drawing.Point(13, 11);
            this.radioButtonExistingTable.Name = "radioButtonExistingTable";
            this.radioButtonExistingTable.Size = new System.Drawing.Size(147, 17);
            this.radioButtonExistingTable.TabIndex = 0;
            this.radioButtonExistingTable.TabStop = true;
            this.radioButtonExistingTable.Text = "Существующая таблица";
            this.radioButtonExistingTable.UseVisualStyleBackColor = true;
            // 
            // radioButtonExistingGraph
            // 
            this.radioButtonExistingGraph.AutoSize = true;
            this.radioButtonExistingGraph.Location = new System.Drawing.Point(13, 38);
            this.radioButtonExistingGraph.Name = "radioButtonExistingGraph";
            this.radioButtonExistingGraph.Size = new System.Drawing.Size(143, 17);
            this.radioButtonExistingGraph.TabIndex = 1;
            this.radioButtonExistingGraph.TabStop = true;
            this.radioButtonExistingGraph.Text = "Существующий график";
            this.radioButtonExistingGraph.UseVisualStyleBackColor = true;
            // 
            // radioButtonNewTableGraph
            // 
            this.radioButtonNewTableGraph.AutoSize = true;
            this.radioButtonNewTableGraph.Location = new System.Drawing.Point(13, 61);
            this.radioButtonNewTableGraph.Name = "radioButtonNewTableGraph";
            this.radioButtonNewTableGraph.Size = new System.Drawing.Size(143, 17);
            this.radioButtonNewTableGraph.TabIndex = 2;
            this.radioButtonNewTableGraph.TabStop = true;
            this.radioButtonNewTableGraph.Text = "Новая таблица/график";
            this.radioButtonNewTableGraph.UseVisualStyleBackColor = true;
            // 
            // radioButtonCaption
            // 
            this.radioButtonCaption.AutoSize = true;
            this.radioButtonCaption.Location = new System.Drawing.Point(13, 84);
            this.radioButtonCaption.Name = "radioButtonCaption";
            this.radioButtonCaption.Size = new System.Drawing.Size(79, 17);
            this.radioButtonCaption.TabIndex = 3;
            this.radioButtonCaption.TabStop = true;
            this.radioButtonCaption.Text = "Заголовок";
            this.radioButtonCaption.UseVisualStyleBackColor = true;
            // 
            // textBoxTable
            // 
            this.textBoxTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTable.Location = new System.Drawing.Point(166, 11);
            this.textBoxTable.Name = "textBoxTable";
            this.textBoxTable.Size = new System.Drawing.Size(229, 20);
            this.textBoxTable.TabIndex = 4;
            this.textBoxTable.TextChanged += new System.EventHandler(this.textBoxTable_TextChanged);
            // 
            // buttonOpenTable
            // 
            this.buttonOpenTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenTable.Location = new System.Drawing.Point(401, 8);
            this.buttonOpenTable.Name = "buttonOpenTable";
            this.buttonOpenTable.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenTable.TabIndex = 5;
            this.buttonOpenTable.Text = "Открыть";
            this.buttonOpenTable.UseVisualStyleBackColor = true;
            this.buttonOpenTable.Click += new System.EventHandler(this.buttonOpenTable_Click);
            // 
            // buttonOpenGraph
            // 
            this.buttonOpenGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenGraph.Location = new System.Drawing.Point(401, 34);
            this.buttonOpenGraph.Name = "buttonOpenGraph";
            this.buttonOpenGraph.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenGraph.TabIndex = 7;
            this.buttonOpenGraph.Text = "Открыть";
            this.buttonOpenGraph.UseVisualStyleBackColor = true;
            this.buttonOpenGraph.Click += new System.EventHandler(this.buttonOpenGraph_Click);
            // 
            // textBoxGraph
            // 
            this.textBoxGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxGraph.Location = new System.Drawing.Point(166, 37);
            this.textBoxGraph.Name = "textBoxGraph";
            this.textBoxGraph.Size = new System.Drawing.Size(229, 20);
            this.textBoxGraph.TabIndex = 6;
            this.textBoxGraph.TextChanged += new System.EventHandler(this.textBoxGraph_TextChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(381, 161);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(92, 23);
            this.buttonCancel.TabIndex = 8;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(285, 161);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(90, 23);
            this.buttonOk.TabIndex = 9;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // radioButtonText
            // 
            this.radioButtonText.AutoSize = true;
            this.radioButtonText.Location = new System.Drawing.Point(12, 107);
            this.radioButtonText.Name = "radioButtonText";
            this.radioButtonText.Size = new System.Drawing.Size(55, 17);
            this.radioButtonText.TabIndex = 10;
            this.radioButtonText.TabStop = true;
            this.radioButtonText.Text = "Текст";
            this.radioButtonText.UseVisualStyleBackColor = true;
            // 
            // NewReportItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 196);
            this.Controls.Add(this.radioButtonText);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOpenGraph);
            this.Controls.Add(this.textBoxGraph);
            this.Controls.Add(this.buttonOpenTable);
            this.Controls.Add(this.textBoxTable);
            this.Controls.Add(this.radioButtonCaption);
            this.Controls.Add(this.radioButtonNewTableGraph);
            this.Controls.Add(this.radioButtonExistingGraph);
            this.Controls.Add(this.radioButtonExistingTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(307, 170);
            this.Name = "NewReportItem";
            this.Text = "Выбор типа элемента";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonExistingTable;
        private System.Windows.Forms.RadioButton radioButtonExistingGraph;
        private System.Windows.Forms.RadioButton radioButtonNewTableGraph;
        private System.Windows.Forms.RadioButton radioButtonCaption;
        private System.Windows.Forms.TextBox textBoxTable;
        private System.Windows.Forms.Button buttonOpenTable;
        private System.Windows.Forms.Button buttonOpenGraph;
        private System.Windows.Forms.TextBox textBoxGraph;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.RadioButton radioButtonText;
    }
}