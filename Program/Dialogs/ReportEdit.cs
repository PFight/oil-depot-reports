﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using Borland.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace OilDepot
{
    public partial class ReportEdit : Form
    {
        Report mReport;

        Report getReport()
        {
            return mReport;
        }

        public ReportEdit()
            : this(new Report())
        {
        }

        public ReportEdit(Report report)
        {
            InitializeComponent();
            loadReport(report);
        }

        private void loadReport(Report report)
        {
            mReport = report;
            foreach (var item in report.mItems)
            {
                listBox1.Items.Add(item);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            object oMissing = System.Reflection.Missing.Value;
	        object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */ 

	        //Start Word and create a new document.
	        Word._Application oWord;
	        Word._Document oDoc;
	        oWord = new Word.Application();
            oWord.Visible = false;
            Cursor.Current = Cursors.WaitCursor;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
                 ref oMissing, ref oMissing);

            foreach (IReportItem item in listBox1.Items)
            {
                item.ToWord(oDoc, ref oMissing);
            }
            Cursor.Current = Cursors.Default;
            oWord.Visible = true;            
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            NewReportItem newItemDlg = new NewReportItem();
            NewReportItem.ItemType type = newItemDlg.ShowNewReportItemDialog();
            if (type == NewReportItem.ItemType.Caption)
            {
                CaptionEdit ce = new CaptionEdit();
                if (ce.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    listBox1.SelectedIndex = listBox1.Items.Add(ce.getReportItem());
                }
            }
            else if (type == NewReportItem.ItemType.Text)
            {
                TextEdit ce = new TextEdit();
                if (ce.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    listBox1.SelectedIndex = listBox1.Items.Add(ce.getReportItem());
                }
            }
            else if (type == NewReportItem.ItemType.NewTableGraph)
            {
                TableGraphEdit editWnd = new TableGraphEdit();
                if (editWnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    listBox1.SelectedIndex = listBox1.Items.Add(editWnd.getData());
                }
            }
            else if (type == NewReportItem.ItemType.ExistingGraph)
            {
                BinaryFormatter ser = new BinaryFormatter();
                TableGraphData data = null;
                using(var stream = new FileStream(newItemDlg.getSelectedGraphPath(), FileMode.Open))
                {
                    data = (TableGraphData)ser.Deserialize(stream);
                }
                listBox1.SelectedIndex = listBox1.Items.Add(data);
            }
            else if(type == NewReportItem.ItemType.ExistingTable)
            {
                BinaryFormatter ser = new BinaryFormatter();
                TableGraphData data = null;
                using (var stream = new FileStream(newItemDlg.getSelectedTablePath(), FileMode.Open))
                {
                    data = (TableGraphData)ser.Deserialize(stream);
                }
                listBox1.SelectedIndex = listBox1.Items.Add(data);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                int newSelIndex = listBox1.SelectedIndex - 1;
                if (newSelIndex < 0) newSelIndex = 0;
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                if (listBox1.Items.Count > 0)
                {
                    listBox1.SelectedIndex = newSelIndex;
                }
            }
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > 0)
            {
                int newIndex = listBox1.SelectedIndex - 1;
                var obj = listBox1.Items[listBox1.SelectedIndex];
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.Insert(newIndex, obj);
                listBox1.SelectedIndex = newIndex;
            }
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < listBox1.Items.Count -1)
            {
                int newIndex = listBox1.SelectedIndex + 1;
                var obj = listBox1.Items[listBox1.SelectedIndex];
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.Insert(newIndex, obj);
                listBox1.SelectedIndex = newIndex;
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                var obj = listBox1.Items[listBox1.SelectedIndex];
                if (obj is CaptionReportItem)
                {
                    CaptionEdit ce = new CaptionEdit(obj as CaptionReportItem);
                    if (ce.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        int index = listBox1.SelectedIndex;
                        listBox1.Items.RemoveAt(index);
                        listBox1.Items.Insert(index, ce.getReportItem());
                        listBox1.SelectedIndex = index;
                    }
                }
                else if (obj is TextReportItem)
                {
                    TextEdit ce = new TextEdit(obj as TextReportItem);
                    if (ce.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        int index = listBox1.SelectedIndex;
                        listBox1.Items.RemoveAt(index);
                        listBox1.Items.Insert(index, ce.getReportItem());
                        listBox1.SelectedIndex = index;
                    }
                }
                else if (obj is TableGraphData)
                {
                    TableGraphEdit ce = new TableGraphEdit((obj as TableGraphData).getSettings());
                    if (ce.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        int index = listBox1.SelectedIndex;
                        listBox1.Items.RemoveAt(index);
                        listBox1.Items.Insert(index, ce.getData());
                        listBox1.SelectedIndex = index;
                    }
                }
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ReportEdit_FormClosed(object sender, FormClosedEventArgs e)
        {
            syncData();
        }

        private void syncData()
        {
            mReport.mItems.Clear();
            foreach (IReportItem item in listBox1.Items)
            {
                mReport.mItems.Add(item);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            syncData();

            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Report files (*.rpt)|*.rpt|All files (*.*)|*.*";
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BinaryFormatter ser = new BinaryFormatter();
                this.Cursor = Cursors.WaitCursor;
                using (var stream = new FileStream(saveFile.FileName, FileMode.Create))
                {
                    ser.Serialize(stream, mReport);
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                textBox1.Text = (listBox1.SelectedItem as IReportItem).GetInfo();
            }
        }

      
    }
}
