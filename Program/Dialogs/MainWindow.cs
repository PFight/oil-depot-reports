﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Borland.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace OilDepot
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            
            TableGraphSettings s = new TableGraphSettings();
            s.mTimeIntervalFrom = DateTime.Now.AddDays(-10);
            s.mTimeIntervalTo = DateTime.Now.AddDays(10);
            s.mGroupEnabled = false;
            SensorInfo sensor = new SensorInfo();
            sensor.mSensorNumber = 1;
            sensor.mTankNumber = 2;
            s.mSensors.Add(sensor);

            
            TAdoDbxConnectionStringBuilder conStr = new TAdoDbxConnectionStringBuilder();
            conStr.HostName = "localhost";
            conStr.Database = "Readouts";
            conStr.User_Name = "SYSDBA";
            conStr.Password = "masterkey";
            conStr.DriverName = "Interbase";
            conStr.MetaDataAssemblyLoader = "Borland.Data.TDBXInterbaseMetaDataCommandFactory," +
                "Borland.Data.DbxReadOnlyMetaData,Version=11.0.5000.0,Culture=neutral," +
                "PublicKeyToken=91d62ebb5b0d1b1b";
            conStr.VendorLib = "GDS32.DLL";
            conStr.GetDriverFunc = "getSQLDriverINTERBASE";
            conStr.LibraryName = "dbxint30.dll";

            TAdoDbxConnection con = new TAdoDbxConnection(conStr.ToString());
            con.Open();
            TableGraphData data = TableGraphData.loadFromDataBase(con, s);
            con.Close();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            TableGraphEdit editWnd = new TableGraphEdit();
            editWnd.MdiParent = this;
            editWnd.Show();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ReportEdit editWnd = new ReportEdit();
            editWnd.MdiParent = this;
            editWnd.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "All supported files (*.tbl, *.grh, *.rpt)|*.tbl;*grh;*.rpt|Report files (*.rpt)|*.rpt|Table files (*.tbl)|*.tbl|Graph files (*.grh)|*.grh|All files (*.*)|*.*";
            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BinaryFormatter ser = new BinaryFormatter();
                this.Cursor = Cursors.WaitCursor;
                using (var stream = new FileStream(openFile.FileName, FileMode.Open))
                {
                    object data = ser.Deserialize(stream);
                    if (data is TableGraphData)
                    {
                        TableGraphEdit edit = new TableGraphEdit((data as TableGraphData).getSettings());
                        edit.MdiParent = this;
                        edit.Show();
                    }
                    else if(data is Report)
                    {
                        ReportEdit edit = new ReportEdit(data as Report);
                        edit.MdiParent = this;
                        edit.Show();
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }
    }
}
