﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace OilDepot
{
    public partial class NewReportItem : Form
    {
        bool cancel = false;
        string mSelectedTablePath = "";
        string mSelectedGraphPath = "";

        public string getSelectedTablePath() { return mSelectedTablePath; } 
        public string getSelectedGraphPath() { return mSelectedGraphPath; }

        public enum ItemType
        {
            Nope,
            ExistingTable,
            ExistingGraph,
            NewTableGraph,
            Caption,
            Text
        }



        public NewReportItem()
        {
            InitializeComponent();
        }

        public ItemType ShowNewReportItemDialog()
        {
            cancel = false;

            show:
            ShowDialog();

            if (cancel) return ItemType.Nope;

            if (radioButtonExistingTable.Checked)
            {
                if (!File.Exists(textBoxTable.Text))
                {
                    MessageBox.Show("Выберите, пожалуйста, существующую таблицу.");
                    goto show;
                }
                return ItemType.ExistingTable;
            }
            else if (radioButtonExistingGraph.Checked)
            {
                if (!File.Exists(textBoxGraph.Text))
                {
                    MessageBox.Show("Выберите, пожалуйста, существующий график.");
                    goto show;
                }
                return ItemType.ExistingGraph;
            }
            else if (radioButtonNewTableGraph.Checked)
            {
                return ItemType.NewTableGraph;
            }
            else if (radioButtonCaption.Checked) return ItemType.Caption;
            else if (radioButtonText.Checked) return ItemType.Text;
            else return ItemType.Nope;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            cancel = true;
            Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonOpenTable_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Table files (*.tbl)|*.tbl|All files(*.*)|*.*";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxTable.Text = dlg.FileName;
            }
        }

        private void buttonOpenGraph_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Graph files (*.grh)|*.grh|All files(*.*)|*.*";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxGraph.Text = dlg.FileName;
            }
        }

        private void textBoxTable_TextChanged(object sender, EventArgs e)
        {
            mSelectedTablePath = textBoxTable.Text;
        }

        private void textBoxGraph_TextChanged(object sender, EventArgs e)
        {
            mSelectedGraphPath = textBoxGraph.Text;
        }
    }
}
