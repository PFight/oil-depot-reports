using System;
using System.Text;
using System.Collections.Generic;
using Borland.Data;

[Serializable]
public enum GroupActions
{
    MeanValue,
    MinValue,
    MaxValue
}

[Serializable]
public enum ItemType
{
    Table,
    Graph
}

[Serializable]
public class SensorInfo
{
    public int mTankNumber;
    public int mSensorNumber;

    public double mUnitCoefficient = 1;
    public double mRangeLowerBound = -10000;
    public double mRangeUpperBound = 10000;
	public string mSensorTypeName = "";
	public string mLocationDescription = "";
	public string mSensorPrecision = "";
	public string mDeviceModel = "";

    public override string ToString()
    {
        return "��������� �" + mTankNumber + " ������ �" + mSensorNumber + " - " + mSensorTypeName;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || !(obj is SensorInfo))
        {
            return false;
        }
        SensorInfo s = obj as SensorInfo;
        return mTankNumber == s.mTankNumber || mSensorNumber == s.mSensorNumber;
    }

    public override int GetHashCode()
    {
        return mTankNumber ^ mSensorNumber;
    }
}

[Serializable]
public class TableGraphSettings
{
    public DateTime mTimeIntervalFrom = DateTime.Now.AddDays(-10);
    public DateTime mTimeIntervalTo = DateTime.Now.AddDays(1);
    public bool mGroupEnabled = false;
    public TimeSpan mGroupTimeStep = new TimeSpan();
    public GroupActions mGroupAction = GroupActions.MeanValue;
    public List<SensorInfo> mSensors = new List<SensorInfo>();
    public ItemType mType = ItemType.Graph;

    public static List<SensorInfo> LoadSensorsInfo(TAdoDbxConnection con)
    {
        List<SensorInfo> sensors = new List<SensorInfo>();
        TAdoDbxCommand query = new TAdoDbxCommand();
        query.CommandText = "SELECT * FROM Sensor";
        query.Connection = con;
        TAdoDbxDataReader reader = (TAdoDbxDataReader)query.ExecuteReader();
        while (reader.Read())
        {
            SensorInfo s = new SensorInfo();
            s.mTankNumber = reader.GetInt32(0);
            s.mSensorNumber = reader.GetInt32(1);
            s.mUnitCoefficient = reader.GetDouble(2);
            s.mRangeLowerBound = reader.GetDouble(4);
            s.mRangeUpperBound = reader.GetDouble(5);
            s.mSensorTypeName = reader.GetString(6);
            s.mLocationDescription = reader.GetString(7);
            s.mSensorPrecision = reader.GetString(8);
            s.mDeviceModel = reader.GetString(9);
            sensors.Add(s);
        }

        return sensors;
    }
}