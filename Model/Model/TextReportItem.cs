﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Xml;

[Serializable]
public class TextReportItem: IReportItem
{
    public int mID = 0;
    public string mText = "";

    public void ToWord(Word._Document oDoc, ref object oMissing)
    {
        Word.Paragraph oPara1;
        oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        oPara1.Range.Text = mText;
        oPara1.Range.Font.Bold = 0;
        oPara1.Range.InsertParagraphAfter();
    }

    public string GetInfo()
    {
        string result = "Текст" + Environment.NewLine + Environment.NewLine + mText;
        return result;
    }

    public override string ToString()
    {
        return "Текст \"" + new String(mText.Take(10).ToArray()) + ((mText.Length > 10) ? "..." : "") + "\"";
    }
}

