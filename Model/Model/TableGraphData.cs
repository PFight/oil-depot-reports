using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Borland.Data;
using System.Xml;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Windows.Forms.DataVisualization.Charting;

[Serializable]
public class Readout 
{
    public SensorInfo mSensor;
    public DateTime mReadingTime;
    public double mReadoutValue;
}

[Serializable]
public class TableGraphData: IReportItem
{
    private TableGraphSettings mSettings;
    private List<Readout> mReadoutValues = new List<Readout>();
    private TableGraphData() {}

    public static TableGraphData loadFromDataBase(TAdoDbxConnection db, TableGraphSettings setting)
    {
        TableGraphData data = new TableGraphData();
        data.loadFromDataBase2(db, setting);
        return data;
    }

    public void loadFromDataBase2(TAdoDbxConnection db, TableGraphSettings setting)
    {
        TableGraphData obj = this;
        obj.mReadoutValues.Clear();
        obj.mSettings = setting;
        foreach(SensorInfo sensor in setting.mSensors)
        {
            TAdoDbxCommand query = new TAdoDbxCommand();
            query.CommandText = "SELECT ReadingTime, ReadoutValue FROM Readout WHERE (ReadingTime >= "+
                "'" + setting.mTimeIntervalFrom.ToString() + "')" +
                "AND (ReadingTime <= '" + setting.mTimeIntervalTo.ToString() + "') AND " +
                " (TankNumber = "+ sensor.mTankNumber.ToString() +") AND (SensorNumber = "+ sensor.mSensorNumber.ToString() +") ORDER BY ReadingTime";

            query.Connection = db;
            TAdoDbxDataReader reader = (TAdoDbxDataReader) query.ExecuteReader();
            while (reader.Read())
            {
                Readout r = new Readout();
                r.mSensor = sensor;
                r.mReadingTime = reader.GetDateTime(0);
                r.mReadoutValue = reader.GetDouble(1);
                obj.mReadoutValues.Add(r);
            }
        }
    }

    public TableGraphSettings getSettings() { return mSettings; }
    public IEnumerable<Readout> getReadoutValues() { return mReadoutValues; }

    private Dictionary<DateTime, List<Readout>> group(List<Readout> readouts)
    {
        Dictionary<DateTime,List<Readout>> result = new Dictionary<DateTime,List<Readout>>();
        
        List<Readout> vals = new List<Readout>(getReadoutValues());
        while (vals.Count > 0)
        {
            DateTime time = vals[0].mReadingTime;
            List<Readout> timeReadouts = new List<Readout>(getSettings().mSensors.Count);
            foreach (Readout r in vals)
            {
                if (r.mReadingTime == time)
                {
                    timeReadouts.Add(r);
                }
            }
            foreach (Readout r in timeReadouts)
            {
                vals.Remove(r);
            }
            result.Add(time, timeReadouts);
        }
        return result;
    }

    public void initTable(DataGridView dataGridView1)
    {
        dataGridView1.Rows.Clear();
        dataGridView1.Columns.Clear();

        DataGridViewTextBoxColumn timeCol = new DataGridViewTextBoxColumn();
        timeCol.ValueType = typeof(DateTime);
        timeCol.Name = "����� ����������";
        timeCol.Frozen = false;
        dataGridView1.Columns.Add(timeCol);
        foreach (SensorInfo sensor in getSettings().mSensors)
        {
            DataGridViewTextBoxColumn sensorCol = new DataGridViewTextBoxColumn();
            sensorCol.ValueType = typeof(double);
            sensorCol.Name = sensor.ToString();
            sensorCol.Tag = sensor;
            sensorCol.Frozen = false;
            dataGridView1.Columns.Add(sensorCol);
        }

        List<Readout> vals = new List<Readout>(getReadoutValues());
        while (vals.Count > 0)
        {
            DateTime time = vals[0].mReadingTime;
            List<Readout> timeReadouts = new List<Readout>(getSettings().mSensors.Count);
            foreach (Readout r in vals)
            {
                if (r.mReadingTime == time)
                {
                    timeReadouts.Add(r);
                }
            }
            foreach (Readout r in timeReadouts)
            {
                vals.Remove(r);
            }
            DataGridViewRow row = new DataGridViewRow();
            DataGridViewTextBoxCell cell = new DataGridViewTextBoxCell();
            cell.Value = time;
            row.Cells.Add(cell);
            for (int sensorIndx = 0; sensorIndx < getSettings().mSensors.Count; sensorIndx++)
            {
                bool found = false;
                foreach (Readout r in timeReadouts)
                {
                    if (r.mSensor == getSettings().mSensors[sensorIndx])
                    {
                        DataGridViewTextBoxCell rcell = new DataGridViewTextBoxCell();
                        rcell.Value = r.mReadoutValue;
                        row.Cells.Add(rcell);
                        found = true;
                    }
                }
                if (!found)
                {
                    DataGridViewTextBoxCell rcell = new DataGridViewTextBoxCell();
                    rcell.Value = null;
                    row.Cells.Add(rcell);
                }
            }
            dataGridView1.Rows.Add(row);
        }

    }

    public void initChart(System.Windows.Forms.DataVisualization.Charting.Chart chart1)
    {
        chart1.Series.Clear();
        List<Readout> vals = new List<Readout>(getReadoutValues());
        while (vals.Count > 0)
        {
            SensorInfo sensor = vals[0].mSensor;
            List<Readout> sensorReadouts = new List<Readout>(vals.Count);
            foreach (Readout r in vals)
            {
                if (r.mSensor == sensor)
                {
                    sensorReadouts.Add(r);
                }
            }
            foreach (Readout r in sensorReadouts)
            {
                vals.Remove(r);
            }

            System.Windows.Forms.DataVisualization.Charting.Series series = new System.Windows.Forms.DataVisualization.Charting.Series();
            series.ChartType = SeriesChartType.Line;
            series.Name = sensor.ToString();
            series.BorderWidth = 4;
            series.XValueType = ChartValueType.DateTime;

            foreach (Readout r in sensorReadouts)
            {
                series.Points.AddXY(r.mReadingTime.ToOADate(), r.mReadoutValue);
            }
            chart1.Series.Add(series);
        }      
    }

    public override string ToString()
    {
        return ((mSettings.mType == ItemType.Table) ? "������� " : "������ ")
            + mSettings.mTimeIntervalFrom.ToShortDateString() + " - " + mSettings.mTimeIntervalTo.ToShortDateString();
    }

    public string GetInfo()
    {
        string result = ((mSettings.mType == ItemType.Table) ? "������� " : "������ ") + Environment.NewLine + Environment.NewLine;
        result += "������ �������: " + mSettings.mTimeIntervalFrom.ToLongDateString() + " " + mSettings.mTimeIntervalFrom.ToLongTimeString() + Environment.NewLine;
        result += "����� �������: " + mSettings.mTimeIntervalTo.ToLongDateString() + " " + mSettings.mTimeIntervalTo.ToLongTimeString() + Environment.NewLine;
        result += "����������� ��������: " + ((mSettings.mGroupEnabled) ? "��" : "���") + Environment.NewLine;
        if (mSettings.mGroupEnabled)
        {
            result += "  ��� �����������: " + mSettings.mGroupTimeStep.ToString() + Environment.NewLine;
            result += "  ��������: ";
            switch (mSettings.mGroupAction)
            {
                case GroupActions.MaxValue: result += "������������ ��������"; break;
                case GroupActions.MinValue: result += "����������� ��������"; break;
                case GroupActions.MeanValue: result += "������� ��������"; break;
            }
            result += Environment.NewLine;
        }
        result += "�������: " + Environment.NewLine;
        foreach (SensorInfo sensor in mSettings.mSensors)
        {
            result += "- " + sensor.ToString() + Environment.NewLine +
                "  ������ ������� ���������: " + sensor.mRangeLowerBound + Environment.NewLine +
                "  ������� ������� ���������: " + sensor.mRangeUpperBound + Environment.NewLine +
                "  ��������: " + sensor.mSensorPrecision + Environment.NewLine +
                "  ������ �������: " + sensor.mDeviceModel + Environment.NewLine;
        }
        
        return result;
    }

    public void ToWord(Word._Document oDoc, ref object oMissing)
    {
        object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */ 

        if (mSettings.mType == ItemType.Table)
        {
            Word.Table oTable;
            Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;

            Dictionary<DateTime, List<Readout>> groupedValues = group(mReadoutValues);


            oTable = oDoc.Tables.Add(wrdRng, groupedValues.Count+1, mSettings.mSensors.Count+1, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;

            // Set headers
            // Indexes from 1!
            oTable.Cell(1, 1).Range.Text = "����� ����������";
            for (int i = 0; i < mSettings.mSensors.Count; i++)
            {
                oTable.Cell(1, i+2).Range.Text = mSettings.mSensors[i].ToString();
            }

            // Fill data
            int rowNumber = 2;
            foreach (List<Readout> timeReadouts in groupedValues.Values)
            {
                // These magic for determine order of readouts, according to getSettings().mSensors
                for (int sensorIndx = 0; sensorIndx < getSettings().mSensors.Count; sensorIndx++)
                {
                    foreach (Readout r in timeReadouts)
                    {
                        if (r.mSensor == getSettings().mSensors[sensorIndx])
                        {
                            oTable.Cell(rowNumber, sensorIndx + 2).Range.Text = r.mReadoutValue.ToString();
                            oTable.Cell(rowNumber, 1).Range.Text = r.mReadingTime.ToString();
                        }
                    }                   
                }
                rowNumber++;
            }
            
            oTable.Rows[1].Range.Font.Bold = 1;
            oTable.Rows[1].Range.Font.Italic = 1;  
          
            // Insert paragraph to split tables
            Word.Paragraph oPara1;
            oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        }
        else if (mSettings.mType == ItemType.Graph)
        {
            Object wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            Chart chart = new Chart();
            chart.ChartAreas.Add(new ChartArea());
            chart.Legends.Add(new Legend());
            chart.Width = 800;
            chart.Height = 600;
            initChart(chart);
            string tempImg = System.IO.Path.GetTempFileName()+".png";
            chart.SaveImage(tempImg, ChartImageFormat.Png);
            oDoc.InlineShapes.AddPicture(tempImg, ref oMissing, ref oMissing, ref wrdRng);

            Word.Paragraph oPara1;
            oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        }
    }
}