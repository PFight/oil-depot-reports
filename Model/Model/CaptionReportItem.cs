﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Xml;


[Serializable]
public class CaptionReportItem: IReportItem
{
    public int mID;
    public string mText = "";

    public void ToWord(Word._Document oDoc, ref object oMissing)
    {
        Word.Paragraph oPara1;
        oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        oPara1.Range.Text = "";
        oPara1.Range.Font.Bold = 0;
        oPara1.Range.InsertParagraphAfter();
        
        oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        oPara1.Range.Text = mText;
        oPara1.Range.Font.Bold = 1;
        oPara1.Format.SpaceAfter = 12;    //24 pt spacing after paragraph.
        oPara1.Format.SpaceBefore = 12;
        oPara1.Range.InsertParagraphAfter();

        oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        oPara1.Range.Text = "";
        oPara1.Range.Font.Bold = 0;
        oPara1.Range.InsertParagraphAfter();
    }

    public string GetInfo()
    {
        string result = "Заголовок" + Environment.NewLine + Environment.NewLine + mText;
        return result;
    }

    public override string ToString()
    {
        return "Заголовок \"" + new String(mText.Take(10).ToArray()) + ((mText.Length > 10) ? "..." : "") + "\"";
    }
}

