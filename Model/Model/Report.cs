﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Xml;

[Serializable]
public class Report
{
    public string mName = "";
    public DateTime mCreationTime = DateTime.MinValue;
    public List<IReportItem> mItems = new List<IReportItem>();
}

