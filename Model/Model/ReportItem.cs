using System;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Xml;

public interface IReportItem
{
    void ToWord(Word._Document doc, ref object oMissing);
    string GetInfo();
}