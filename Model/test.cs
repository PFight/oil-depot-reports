﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using OilDepot;


namespace SiteLogic
{
    public class Test: Page
    {
        T Find<T>(string id) where T:Control
        {
            foreach (Control cnt in Controls)
            {
                if (cnt.ID == id)
                {
                    return (T)cnt;
                }
            }
            return null;
        }

        
        protected override void OnLoad(EventArgs e)
        {
            Response.Write("Sensors: ");

            
            TableGraphSettings set = new TableGraphSettings();
            using (var con = DataBaseManager.OpenConnection())
            {
                foreach (SensorInfo s in TableGraphSettings.LoadSensorsInfo(con))
                {
                    Response.Write(s.ToString() + "<br/>");
                }
            }

            Response.Write("<br/><br/>");

            Response.Write("From dll time: " + System.DateTime.Now.ToString());
            base.OnLoad(e);
        }
    }
}
