﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Borland.Data;

namespace OilDepot
{
    public class DataBaseManager
    {
        public static TAdoDbxConnection OpenConnection()
        {
            TAdoDbxConnectionStringBuilder conStr = new TAdoDbxConnectionStringBuilder();
            conStr.HostName = "localhost";
            conStr.Database = "Readouts";
            conStr.User_Name = "SYSDBA";
            conStr.Password = "masterkey";
            conStr.DriverName = "Interbase";
            conStr.MetaDataAssemblyLoader = "Borland.Data.TDBXInterbaseMetaDataCommandFactory," +
                "Borland.Data.DbxReadOnlyMetaData,Version=11.0.5000.0,Culture=neutral," +
                "PublicKeyToken=91d62ebb5b0d1b1b";
            conStr.VendorLib = "GDS32.DLL";
            conStr.GetDriverFunc = "getSQLDriverINTERBASE";
            conStr.LibraryName = "dbxint30.dll";

            TAdoDbxConnection con = new TAdoDbxConnection(conStr.ToString());
            con.Open();
            return con;
        }
        public static TAdoDbxConnection OpenSiteConnection()
        {
            TAdoDbxConnectionStringBuilder conStr = new TAdoDbxConnectionStringBuilder();
            conStr.HostName = "localhost";
            conStr.Database = "Reports";
            conStr.User_Name = "SYSDBA";
            conStr.Password = "masterkey";
            conStr.DriverName = "Interbase";
            conStr.MetaDataAssemblyLoader = "Borland.Data.TDBXInterbaseMetaDataCommandFactory," +
                "Borland.Data.DbxReadOnlyMetaData,Version=11.0.5000.0,Culture=neutral," +
                "PublicKeyToken=91d62ebb5b0d1b1b";
            conStr.VendorLib = "GDS32.DLL";
            conStr.GetDriverFunc = "getSQLDriverINTERBASE";
            conStr.LibraryName = "dbxint30.dll";

            TAdoDbxConnection con = new TAdoDbxConnection(conStr.ToString());
            con.Open();
            return con;
        }

    }
}
