﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Borland.Data;

namespace GetReadouts
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var con = OilDepot.DataBaseManager.OpenConnection())
            {
                // Get last readout date in data base
                TAdoDbxCommand getMaxDate = (TAdoDbxCommand)con.CreateCommand();
                getMaxDate.CommandText = "SELECT MAX(ReadingTime) FROM Readout";
                TAdoDbxDataReader reader = (TAdoDbxDataReader)getMaxDate.ExecuteReader();
                DateTime maxDate;
                if (reader.Read())
                {
                    maxDate = reader.GetDateTime(0);
                }
                else
                {
                    maxDate = DateTime.MinValue;
                }

                maxDate = maxDate.Date;

                string logDir = File.ReadAllText("LogDirectory.txt").Trim();
                string[] logs = Directory.GetFiles(logDir, "*.slog");

                // Get files, that oldier or equal to maxDate
                List<string> listlogs = new List<string>(logs);
                var dates = listlogs.ConvertAll(str => DateTime.Parse(Path.GetFileNameWithoutExtension(str)));
                dates.Sort();
                var datesToProcess = dates.SkipWhile(date => date < maxDate);

                // Get new records from these logs
                foreach (DateTime date in datesToProcess)
                {
                    string logFileName = Path.Combine(logDir, date.ToShortDateString() + ".slog");
                    using (StreamReader file = new StreamReader(logFileName))
                    {
                        while (!file.EndOfStream)
                        {
                            string row = file.ReadLine();
                            string[] rowParts = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (rowParts.Length == 4)
                            {
                                DateTime time = DateTime.Parse(rowParts[0]);
                                DateTime dateTime = date + (time - DateTime.Today);
                                int tankNumber = int.Parse(rowParts[1]);
                                int sensorNumber = int.Parse(rowParts[2]);
                                double readoutValue = double.Parse(rowParts[3]);

                                // If there is no such readout, insert new recort to data base
                                TAdoDbxCommand insert = (TAdoDbxCommand)con.CreateCommand();
                                insert.CommandText = "INSERT INTO Readout (ReadingTime, TankNumber, SensorNumber, ReadoutValue) " +
                                    "VALUES('" + dateTime.ToString() + "', " + tankNumber + ", " + sensorNumber + ", " + readoutValue + ")";
                                Console.WriteLine(insert.CommandText);
                                try
                                {
                                    int affected = insert.ExecuteNonQuery();
                                    Console.WriteLine("Affected " + affected);
                                }
                                catch (TAdoDbxException)
                                {
                                    Console.WriteLine("Already exists");
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
