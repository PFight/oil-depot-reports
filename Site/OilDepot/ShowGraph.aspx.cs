﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Web.UI.DataVisualization.Charting;

namespace OilDepot
{
    public partial class ShowGraph : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mData"] == null)
            {
                TableGraphSettings mSettings = Session["mSettings"] as TableGraphSettings;
                if (mSettings != null)
                {
                    using (var con = DataBaseManager.OpenConnection())
                    {
                        TableGraphData mData = TableGraphData.loadFromDataBase(con, mSettings);
                        Session["mData"] = mData;
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                TableGraphSettings mSettings = Session["mSettings"] as TableGraphSettings;
                using (var con = DataBaseManager.OpenConnection())
                {
                    (Session["mData"] as TableGraphData).loadFromDataBase2(con, mSettings);
                }
            }

            initChart(Session["mData"] as TableGraphData);
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("GraphTableEdit.aspx");
        }

        public void initChart(TableGraphData data)
        {
            Chart1.Series.Clear();
            List<Readout> vals = new List<Readout>(data.getReadoutValues());
            while (vals.Count > 0)
            {
                SensorInfo sensor = vals[0].mSensor;
                List<Readout> sensorReadouts = new List<Readout>(vals.Count);
                foreach (Readout r in vals)
                {
                    if (r.mSensor == sensor)
                    {
                        sensorReadouts.Add(r);
                    }
                }
                foreach (Readout r in sensorReadouts)
                {
                    vals.Remove(r);
                }

                System.Web.UI.DataVisualization.Charting.Series series = new Series();
                series.ChartType = SeriesChartType.Line;
                series.Name = sensor.ToString();
                series.BorderWidth = 4;
                series.XValueType = ChartValueType.DateTime;

                sensorReadouts.Sort((r1, r2) => r1.mReadingTime.CompareTo(r2.mReadingTime));


                foreach (Readout r in sensorReadouts)
                {
                    series.Points.AddXY(r.mReadingTime.ToOADate(), r.mReadoutValue);
                }
                Chart1.Series.Add(series);

                Chart1.Width = 800;
                Legend leg = new Legend();
                Chart1.Legends.Add(leg);
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            BinaryFormatter ser = new BinaryFormatter();
            string file = Request.MapPath("graph.grh");
            using (var stream = new FileStream(file, FileMode.Create))
            {
                ser.Serialize(stream, Session["mData"]);
            }
            Response.Redirect("graph.grh", true);
        }

        // Кнопка Готово
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["PreviousPage"] != null)
            {
                Session["DialogResult"] = true;
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

    }
}