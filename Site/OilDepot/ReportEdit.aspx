﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportEdit.aspx.cs" Inherits="OilDepot.ReportEdit" MasterPageFile="Site.Master" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
        <table>
            <tr>
                <td>
                    <h3>Название отчета:</h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox Width="100%" ID="TextBoxName" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="ButtonSave" runat="server" Text="Сохранить & закрыть"  
                        Width="153px" onclick="ButtonSave_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <h3>Элементы отчета:</h3>
                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:PlaceHolder ID="ReportElements" runat="server"></asp:PlaceHolder>
                </td>
            </tr>
            <tr>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%">
                        <tr>
                            <td>
                                <asp:Button width="100%" ID="ButtonAddTableGraph" runat="server" 
                                    Text="Добавить таблицу/график" onclick="ButtonAddTableGraph_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button width="100%" ID="ButtonAddHeader" runat="server" Text="Добавить заголовок" 
                                    onclick="ButtonAddHeader_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button width="100%" ID="ButtonText" runat="server" Text="Добавить текст" 
                                    onclick="ButtonText_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td align="right" valign="top">
                    <asp:Button ID="ButtonExport" runat="server" Text="Скачать как документ Word" 
                        onclick="ButtonExport_Click" Width="192px"  />
                </td>
            </tr>
        </table>
</asp:Content>