﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Borland.Data;

namespace OilDepot
{
    public partial class ReportList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UpdateList();
            }
        }

        private void UpdateList()
        {
            ListBox1.Items.Clear();
            using (var con = DataBaseManager.OpenSiteConnection())
            {
                TAdoDbxCommand query = con.CreateCommand() as TAdoDbxCommand;
                query.CommandText = "SELECT Report.ReportName, COUNT(*) " +
                    "FROM Report LEFT JOIN ReportElement ON Report.ReportName = ReportElement.ReportName " + 
                    "GROUP BY Report.ReportName, Report.CreationTime " +
                    "ORDER BY Report.CreationTime DESC";
                TAdoDbxDataReader reader = (TAdoDbxDataReader)query.ExecuteReader();
                while (reader.Read())
                {
                    ListItem item = new ListItem(reader.GetString(0) + "(элементов: " + reader[1].ToString() + ")", reader.GetString(0));
                    ListBox1.Items.Add(item);
                }
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            Session["mReport"] = null;
            Response.Redirect("ReportEdit.aspx");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedItem != null)
            {
                using (var con = DataBaseManager.OpenSiteConnection())
                {
                    TAdoDbxCommand query = con.CreateCommand() as TAdoDbxCommand;
                    query.CommandText = "DELETE FROM Report WHERE ReportName = \"" + ListBox1.SelectedValue + "\"";
                    query.ExecuteNonQuery();
                }
                UpdateList();
            }
        }

        protected void ButtonSelect_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedItem != null)
            {
                Report report = new Report();
                report.mName = ListBox1.SelectedValue;

                using (var siteCon = DataBaseManager.OpenSiteConnection())
                {
                    // Query creation time
                    TAdoDbxCommand queryCreationTime = siteCon.CreateCommand() as TAdoDbxCommand;
                    queryCreationTime.CommandText = "SELECT CreationTime FROM Report WHERE ReportName = \"" + report.mName + "\"";
                    TAdoDbxDataReader readerCreationTime = (TAdoDbxDataReader)queryCreationTime.ExecuteReader();
                    if (readerCreationTime.Read())
                    {
                        report.mCreationTime = readerCreationTime.GetDateTime(0);
                    }

                    // Load report items
                    TAdoDbxCommand querySelectReportItem = siteCon.CreateCommand() as TAdoDbxCommand;
                    querySelectReportItem.CommandText = "SELECT elementType, Text, IntervalBegin, IntervalEnd, reportName, elementID" +
                        " FROM ReportElement WHERE (reportName = \"" + report.mName + "\")";
                    TAdoDbxDataReader readerReportItem = (TAdoDbxDataReader)querySelectReportItem.ExecuteReader();
                    List<TableGraphSettings> loadedSettings = new List<TableGraphSettings>();
                    while (readerReportItem.Read())
                    {
                        if (readerReportItem.GetString(0) == "table" || readerReportItem.GetString(0) == "graph")
                        {
                            TableGraphSettings settings = new TableGraphSettings();
                            settings.mType = (readerReportItem.GetString(0) == "table") ? ItemType.Table : ItemType.Graph;
                            settings.mName = readerReportItem.GetString(1);
                            settings.mTimeIntervalFrom = readerReportItem.GetDateTime(2);
                            settings.mTimeIntervalTo = readerReportItem.GetDateTime(3);
                            settings.mID = readerReportItem.GetInt32(5);
                            loadedSettings.Add(settings);
                        }
                        else if (readerReportItem.GetString(0) == "caption")
                        {
                            CaptionReportItem caption = new CaptionReportItem();
                            caption.mID = readerReportItem.GetInt32(5);
                            caption.mText = readerReportItem.GetString(1);
                            report.mItems.Add(caption);
                        }
                        else if (readerReportItem.GetString(0) == "text")
                        {
                            TextReportItem text = new TextReportItem();
                            text.mID = readerReportItem.GetInt32(5);
                            text.mText = readerReportItem.GetString(1);
                            report.mItems.Add(text);
                        }
                    }

                    using (var con2 = DataBaseManager.OpenConnection())
                    {
                        foreach (TableGraphSettings set in loadedSettings)
                        {
                            // Load sensors
                            TAdoDbxCommand querySelectSensorID = siteCon.CreateCommand() as TAdoDbxCommand;
                            querySelectSensorID.CommandText = "SELECT sensorNumber, tankNumber FROM TableGraphSensor WHERE " +
                                "elementID = \"" + set.mID + "\"";
                            TAdoDbxDataReader readerSensorID = (TAdoDbxDataReader)querySelectSensorID.ExecuteReader();
                            while (readerSensorID.Read())
                            {
                                TAdoDbxCommand querySensorInfo = con2.CreateCommand() as TAdoDbxCommand;
                                querySensorInfo.CommandText = "SELECT * FROM Sensor WHERE " +
                                    "(sensorNumber = \"" + readerSensorID[0].ToString() + "\") AND (tankNumber = \"" +
                                    readerSensorID[1].ToString() + "\")";
                                TAdoDbxDataReader readerSensorInfo = (TAdoDbxDataReader)querySensorInfo.ExecuteReader();
                                if (readerSensorInfo.Read())
                                {
                                    SensorInfo sens = new SensorInfo();
                                    sens.mTankNumber = readerSensorInfo.GetInt32(0);
                                    sens.mSensorNumber = readerSensorInfo.GetInt32(1);
                                    sens.mUnitCoefficient = readerSensorInfo.GetDouble(2);
                                    sens.mRangeLowerBound = readerSensorInfo.GetDouble(4);
                                    sens.mRangeUpperBound = readerSensorInfo.GetDouble(5);
                                    sens.mSensorTypeName = readerSensorInfo.GetString(6);
                                    sens.mLocationDescription = readerSensorInfo.GetString(7);
                                    sens.mSensorPrecision = readerSensorInfo.GetString(8);
                                    sens.mDeviceModel = readerSensorInfo.GetString(9);
                                    set.mSensors.Add(sens);
                                }
                            }
                        }
                        foreach (TableGraphSettings set in loadedSettings)
                        {
                            TableGraphData data = TableGraphData.loadFromDataBase(con2, set);
                            if (data != null)
                            {
                                report.mItems.Add(data);
                            }
                        }
                    }
                }

                Session["mReport"] = report;
                Response.Redirect("ReportEdit.aspx");
            }
        }
    }
}