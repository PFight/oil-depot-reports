﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OilDepot
{
    public partial class HeaderEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mHeader"] == null)
            {
                CaptionReportItem set = new CaptionReportItem();
                setCaption(set);
                Session.Add("mHeader", set);
            }
            else if (!IsPostBack)
            {
                setCaption(Session["mHeader"] as CaptionReportItem);
            }
        }

        private void setCaption(CaptionReportItem cap)
        {
            TextBoxCaption.Text = cap.mText;
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Session["mHeader"] = null;

            if (Session["PreviousPage"] != null)
            {
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void ButtonOk_Click(object sender, EventArgs e)
        {
            (Session["mHeader"] as CaptionReportItem).mText = TextBoxCaption.Text;

            if (Session["PreviousPage"] != null)
            {
                Session["DialogResult"] = true;
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }


    }
}