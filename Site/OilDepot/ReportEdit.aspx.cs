﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Borland.Data;

namespace OilDepot
{
    public partial class ReportEdit : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mReport"] == null)
            {
                Report rep = new Report();
                rep.mName = "Отчет " + DateTime.Now.ToString();
                rep.mCreationTime = DateTime.Now;

                loadReport(rep);

                Session.Add("mReport", rep);
            }
            else
            {
                Report rep = Session["mReport"] as Report;
                if (Session["DialogResult"] != null)
                {
                    if (Session["mData"] != null)
                    {
                        TableGraphData data = Session["mData"] as TableGraphData;
                        if (!rep.mItems.Contains(data))
                        {
                            rep.mItems.Add(data);
                        }
                        Session["mData"] = null;
                        Session["mSettings"] = null;
                    }
                    if (Session["mHeader"] != null)
                    {
                        CaptionReportItem cap = Session["mHeader"] as CaptionReportItem;
                        if (!rep.mItems.Contains(cap))
                        {
                            rep.mItems.Add(cap);
                        }
                        Session["mHeader"] = null;
                    }
                    if (Session["mText"] != null)
                    {
                        TextReportItem txt = Session["mText"] as TextReportItem;
                        if (!rep.mItems.Contains(txt))
                        {
                            rep.mItems.Add(txt);
                        }
                        Session["mText"] = null;
                    }
                    Session["DialogResult"] = null;
                }
                loadReport(Session["mReport"] as Report);
            }

            if (!IsPostBack)
            {
                TextBoxName.Text = (Session["mReport"] as Report).mName;
            }
        }

        private void loadReport(Report rep)
        {
            ReportElements.Controls.Clear();

            int i = 0;
            foreach (IReportItem item in rep.mItems)
            {
                Panel p = new Panel();
                
                Literal info = new Literal();
                info.Text = "<b>" + item.ToString() + "</b><br/>";
                p.Controls.Add(info);

                Button edit = new Button();
                edit.Text = "Редактировать";
                edit.ID = i.ToString();
                edit.Click += ButtonEditItem_Click;
                edit.BorderStyle = BorderStyle.None;
                p.Controls.Add(edit);

                Button del = new Button();
                del.Text = "Удалить";
                del.ID = " " + i.ToString();
                del.Click += ButtonDelete_Click;
                del.BorderStyle = BorderStyle.None;
                p.Controls.Add(del);

                if (i > 0)
                {
                    Button up = new Button();
                    up.Text = "Вверх";
                    up.ID = "  " + i.ToString();
                    up.Click += ButtonUp_Click;
                    up.BorderStyle = BorderStyle.None;
                    p.Controls.Add(up);
                }

                if (i < rep.mItems.Count - 1)
                {
                    Button down = new Button();
                    down.Text = "Вниз";
                    down.ID = "    " + i.ToString();
                    down.Click += ButtonDown_Click;
                    down.BorderStyle = BorderStyle.None;
                    p.Controls.Add(down);
                }


                Literal hr = new Literal();
                hr.Text = "<hr/>";
                p.Controls.Add(hr);

                ReportElements.Controls.Add(p);
                i++;
            }

            if (rep.mItems.Count() == 0)
            {
                Literal hr = new Literal();
                hr.Text = "<i>Нет элементов</i>";
                ReportElements.Controls.Add(hr);
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Session["mReport"] = null;
            Response.Redirect("Default.aspx");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Button btn = sender as Button;
            Report rep = Session["mReport"] as Report;
            rep.mItems.RemoveAt(int.Parse(btn.ID));
            loadReport(rep);
        }

        protected void ButtonUp_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Button btn = sender as Button;
            Report rep = Session["mReport"] as Report;
            int oldIndx = int.Parse(btn.ID);
            if (oldIndx > 0)
            {
                IReportItem item = rep.mItems[oldIndx];
                rep.mItems.RemoveAt(oldIndx);
                rep.mItems.Insert(oldIndx - 1, item);
            }
            loadReport(rep);            
        }

        protected void ButtonDown_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Button btn = sender as Button;
            Report rep = Session["mReport"] as Report;
            int oldIndx = int.Parse(btn.ID);
            if (oldIndx < rep.mItems.Count-1)
            {
                IReportItem item = rep.mItems[oldIndx];
                rep.mItems.RemoveAt(oldIndx);
                rep.mItems.Insert(oldIndx + 1, item);
            }
            loadReport(rep);            
        }

        protected void ButtonEditItem_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Button btn = sender as Button;
            Report rep = Session["mReport"] as Report;
            IReportItem item = rep.mItems[int.Parse(btn.ID)];
            Session["PreviousPage"] = "ReportEdit.aspx";
            Session["DialogResult"] = null;
            if (item is TableGraphData)
            {
                Session["mSettings"] = (item as TableGraphData).getSettings();
                Session["mData"] = (item as TableGraphData);
                Response.Redirect("GraphTableEdit.aspx");
            }
            else if (item is CaptionReportItem)
            {
                Session["mHeader"] = (item as CaptionReportItem);
                Response.Redirect("HeaderEdit.aspx");
            }
            else if (item is TextReportItem)
            {
                Session["mText"] = (item as TextReportItem);
                Response.Redirect("TextEdit.aspx");
            }
        }

        protected void ButtonExport_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            Microsoft.Office.Interop.Word._Application oWord;
            Microsoft.Office.Interop.Word._Document oDoc;
            oWord = new Microsoft.Office.Interop.Word.Application();
            oWord.Visible = false;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
                 ref oMissing, ref oMissing);

            Report rep = Session["mReport"] as Report;
            foreach (IReportItem item in rep.mItems)
            {
                item.ToWord(oDoc, ref oMissing);
            }
            string file = Request.MapPath("report.docx");
            if (File.Exists(file)) File.Delete(file);
            oDoc.SaveAs2(file);
            oWord.Quit();
            System.Threading.Thread.Sleep(500);
            Response.Redirect("report.docx", true);
        }

        protected void ButtonAddTableGraph_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Session["PreviousPage"] = "ReportEdit.aspx";
            Session["mSettings"] = null;
            Session["mData"] = null;
            Session["DialogResult"] = null;
            Response.Redirect("GraphTableEdit.aspx");
        }

        protected void ButtonAddHeader_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Session["PreviousPage"] = "ReportEdit.aspx";
            Session["mHeader"] = null;
            Session["DialogResult"] = null;
            Response.Redirect("HeaderEdit.aspx");
        }

        protected void ButtonText_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;
            Session["PreviousPage"] = "ReportEdit.aspx";
            Session["mText"] = null;
            Session["DialogResult"] = null;
            Response.Redirect("TextEdit.aspx");
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            (Session["mReport"] as Report).mName = TextBoxName.Text;

            using (var siteCon = DataBaseManager.OpenSiteConnection())
            {
                Report rep = Session["mReport"] as Report;

                TAdoDbxCommand queryExists = siteCon.CreateCommand() as TAdoDbxCommand;
                queryExists.CommandText = "SELECT ReportName FROM Report WHERE ReportName = \"" + rep.mName + "\"";
                TAdoDbxDataReader readerExists = (TAdoDbxDataReader)queryExists.ExecuteReader();
                bool exists = readerExists.HasRows;
                if (exists)
                {
                    TAdoDbxCommand queryDel = siteCon.CreateCommand() as TAdoDbxCommand;
                    queryDel.CommandText = "DELETE FROM Report WHERE ReportName = \"" + rep.mName + "\"";
                    queryDel.ExecuteNonQuery();
                }

                rep.mName = TextBoxName.Text;
                if (rep.mCreationTime == DateTime.MinValue)
                {
                    rep.mCreationTime = DateTime.Now;
                }

                TAdoDbxCommand queryInsRep = siteCon.CreateCommand() as TAdoDbxCommand;
                queryInsRep.CommandText = "INSERT INTO Report (ReportName, CreationTime) VALUES (\"" 
                    + rep.mName + "\", \"" + rep.mCreationTime.ToString() + "\")";
                queryInsRep.ExecuteReader();

                foreach (IReportItem item in rep.mItems)
                {
                    TAdoDbxCommand queryMaxID = siteCon.CreateCommand() as TAdoDbxCommand;
                    queryMaxID.CommandText = "SELECT MAX(ElementID) FROM ReportElement";
                    TAdoDbxDataReader readerMaxID = (TAdoDbxDataReader)queryMaxID.ExecuteReader();
                    int newID = 0;
                    if (readerMaxID.HasRows)
                    {
                        newID = readerMaxID.GetInt32(0) + 1;
                    }

                    if (item is TableGraphData)
                    {
                        TableGraphSettings set = (item as TableGraphData).getSettings();
                        set.mID = newID;
                        TAdoDbxCommand queryInsSet = siteCon.CreateCommand() as TAdoDbxCommand;
                        queryInsSet.CommandText = "INSERT INTO ReportElement (ReportName, ElementType, IntervalBegin, IntervalEnd,  ElementID) VALUES (\""
                            + rep.mName + "\", \"" + set.mType.ToString().ToLower() + "\", \"" + set.mTimeIntervalFrom.ToString() + "\", " + 
                            "\"" + set.mTimeIntervalTo.ToString() + "\",  " + newID.ToString() + ")";
                        queryInsSet.ExecuteReader();

                        foreach (SensorInfo sens in set.mSensors)
                        {
                            TAdoDbxCommand queryInsSens = siteCon.CreateCommand() as TAdoDbxCommand;
                            queryInsSens.CommandText = "INSERT INTO TableGraphSensor (ElementID, SensorNumber, TankNumber) VALUES ("
                                + set.mID + ", " + sens.mSensorNumber + ", " + sens.mTankNumber + ")";
                            queryInsSens.ExecuteReader();
                        }
                    }
                    else if (item is CaptionReportItem)
                    {
                        CaptionReportItem cap = item as CaptionReportItem;
                        TAdoDbxCommand queryInsSet = siteCon.CreateCommand() as TAdoDbxCommand;
                        queryInsSet.CommandText = "INSERT INTO ReportElement (ReportName, ElementType, Text,  ElementID) VALUES (\""
                            + rep.mName + "\", \"caption\", " + 
                            "\"" + cap.mText.ToString() + "\",  " + newID.ToString() + ")";
                        queryInsSet.ExecuteReader();
                    }
                    else if (item is TextReportItem)
                    {
                        TextReportItem cap = item as TextReportItem;
                        TAdoDbxCommand queryInsSet = siteCon.CreateCommand() as TAdoDbxCommand;
                        queryInsSet.CommandText = "INSERT INTO ReportElement (ReportName, ElementType, Text,  ElementID) VALUES (\""
                            + rep.mName + "\", \"text\", " +
                            "\"" + cap.mText.ToString() + "\",  " + newID.ToString() + ")";
                        queryInsSet.ExecuteReader();
                    }
                }
            }

            Session["mReport"] = null;

            Response.Redirect("ReportList.aspx");
        }

    }
}