﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraphTableEdit.aspx.cs" Inherits="OilDepot.GraphTableEdit" MasterPageFile="Site.Master" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
        <table>
            <tr>
                <td align="center">
                    <b>Начиная с:</b> <br />
                    <asp:Calendar ID="CalendarFrom" runat="server" BackColor="White" 
                        BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                        DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#003399" Height="200px" Width="220px" >
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                            Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                    </asp:Calendar>
                </td>
                <td width="5">
                </td>
                <td align="center">
                    <b>Заканчивая: </b><br />
                    <asp:Calendar ID="CalendarTo" runat="server" BackColor="White" 
                        BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                        DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#003399" Height="200px" Width="220px">
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                            Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                    </asp:Calendar>
                   
                </td>
                <td class="style1"></td>                
                <td rowspan="2" valign="top">
                    <strong>Датчики:</strong><br />
                    <asp:CheckBoxList ID="CheckBoxListSensors" runat="server">
                    </asp:CheckBoxList> 
                </td>
            </tr>
            <tr valign="middle">
                <td valign="top" align="center">
                    Время 
                    <asp:TextBox ID="TextBoxFromHour" runat="server" Text="0" Width="44px"></asp:TextBox>
                    :
                    <asp:TextBox ID="TextBoxFromMin" runat="server" Text="0" Width="43px"></asp:TextBox>
                    :
                    <asp:TextBox ID="TextBoxFromSec" runat="server" Text="0" Width="41px"></asp:TextBox> 
                    
                &nbsp;</td>
                <td width="5">&nbsp;
                </td>
                <td valign="top" align="center">
                    Время
                    <asp:TextBox ID="TextBoxToHour" runat="server" Text="0" Width="43px"></asp:TextBox>
                    :
                    <asp:TextBox ID="TextBoxToMin" runat="server" Text="0" Width="44px"></asp:TextBox>
                    :
                    <asp:TextBox ID="TextBoxToSec" runat="server" Text="0" Width="44px"></asp:TextBox> 
                    
                </td>
            </tr>
            
            <tr>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                    <td></td>
                <td align="right">
                    <asp:Button ID="Button2" runat="server" Text="Построить таблицу &gt;&gt;" 
                        onclick="Button2_Click" Width="153px" />
                </td>
            </tr>
            <tr>
                <td>
                  
                    <asp:Button ID="Button1" runat="server" Text="Отмена" onclick="Button1_Click" 
                        Width="108px" />
                  
                </td>
                <td></td>
                <td align="right">
                    <asp:Button ID="Button6" runat="server" Text="Построить график &gt;&gt;" 
                        onclick="ButtonShowGraph_Click" Width="153px" />
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
    .style1
    {
        width: 15px;
    }
</style>
</asp:Content>
