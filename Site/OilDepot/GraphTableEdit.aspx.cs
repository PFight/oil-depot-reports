﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OilDepot
{
    public partial class GraphTableEdit : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mSettings"] == null)
            {
                TableGraphSettings set = new TableGraphSettings();
                set.mTimeIntervalFrom = new DateTime(2012, 12, 29);
                set.mTimeIntervalTo = new DateTime(2013, 1, 10);
                using (var con = DataBaseManager.OpenConnection())
                {
                    foreach (SensorInfo s in TableGraphSettings.LoadSensorsInfo(con))
                    {
                        set.mSensors.Add(s);
                    }
                }
                Session.Add("mSettings", set);
                setSettings(set);
            }
            else if (!IsPostBack)
            {
                setSettings(Session["mSettings"] as TableGraphSettings);    
            }
            
        }

        private void setSettings(TableGraphSettings initSettings)
        {
            TableGraphSettings mSettings = initSettings;

            CalendarFrom.SelectedDate = mSettings.mTimeIntervalFrom;
            CalendarFrom.VisibleDate = CalendarFrom.SelectedDate;
            TextBoxFromHour.Text = mSettings.mTimeIntervalFrom.Hour.ToString();
            TextBoxFromMin.Text = mSettings.mTimeIntervalFrom.Minute.ToString();
            TextBoxFromSec.Text = mSettings.mTimeIntervalFrom.Second.ToString();

            CalendarTo.SelectedDate= mSettings.mTimeIntervalTo;
            CalendarTo.VisibleDate = CalendarTo.SelectedDate;
            TextBoxToHour.Text = mSettings.mTimeIntervalTo.Hour.ToString();
            TextBoxToMin.Text = mSettings.mTimeIntervalTo.Minute.ToString();
            TextBoxToSec.Text = mSettings.mTimeIntervalTo.Second.ToString();

            if (CheckBoxListSensors.Items.Count == 0)
            {
                using (var con = DataBaseManager.OpenConnection())
                {
                    foreach (SensorInfo s in TableGraphSettings.LoadSensorsInfo(con))
                    {
                        string text = s.ToString() + "<br/>" +
                            "Нижняя граница измерений: " + s.mRangeLowerBound + "<br/>" +
                            "Верхняя граница измерений: " + s.mRangeUpperBound + "<br/>" +
                            "Точность: " + s.mSensorPrecision + "<br/>" +
                            "Модель датчика: " + s.mDeviceModel;
                        ListItem item = new ListItem(text, 
                            s.mSensorNumber.ToString() + " " + s.mTankNumber.ToString(), true);
                        item.Selected = mSettings.mSensors.Contains(s);
                        CheckBoxListSensors.Items.Add(item);
                    }
                }
            }
            else
            {
                foreach (SensorInfo sensor in mSettings.mSensors)
                {
                    CheckBoxListSensors.Items.Remove(CheckBoxListSensors.Items.FindByValue(sensor.mSensorNumber.ToString()));
                    string text = sensor.ToString() + "<br/>" +
                            "Нижняя граница измерений: " + sensor.mRangeLowerBound + "<br/>" +
                            "Верхняя граница измерений: " + sensor.mRangeUpperBound + "<br/>" +
                            "Точность: " + sensor.mSensorPrecision + "<br/>" +
                            "Модель датчика: " + sensor.mDeviceModel;
                    ListItem item = new ListItem(text, 
                        sensor.mSensorNumber.ToString() + " " + sensor.mTankNumber.ToString(), true);
                    item.Selected = true;
                    CheckBoxListSensors.Items.Add(item);
                    
                }
            }
        }

        private void someValueChanged(object sender, EventArgs e)
        {
            DateTime dateFrom = CalendarFrom.SelectedDate;
            TableGraphSettings mSettings = Session["mSettings"] as TableGraphSettings;

            mSettings.mTimeIntervalFrom = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day,
                int.Parse(TextBoxFromHour.Text), int.Parse(TextBoxFromMin.Text), int.Parse(TextBoxFromSec.Text));
            DateTime dateTo = CalendarTo.SelectedDate;
            mSettings.mTimeIntervalTo = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day,
                int.Parse(TextBoxToHour.Text), int.Parse(TextBoxToMin.Text), int.Parse(TextBoxToSec.Text));
            mSettings.mSensors.Clear();
            using (var con = DataBaseManager.OpenConnection())
            {
                List<SensorInfo> allSensors = TableGraphSettings.LoadSensorsInfo(con);
                foreach (ListItem sensor in CheckBoxListSensors.Items)
                {
                    if (sensor.Selected)
                    {
                        mSettings.mSensors.Add(allSensors.Find(s => 
                            (s.mSensorNumber.ToString() + " " + s.mTankNumber.ToString()) == sensor.Value));
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["mSettings"] = null;
            Session["mData"] = null;

            if (Session["PreviousPage"] != null)
            {
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void ButtonShow_Click(object sender, EventArgs e)
        {
            using (var con = DataBaseManager.OpenConnection())
            {
                List<SensorInfo> allSensors = TableGraphSettings.LoadSensorsInfo(con);
                foreach (ListItem ss in CheckBoxListSensors.Items)
                {
                    if (ss.Selected)
                    {
                        SensorInfo sensor = allSensors.Find(s =>
                                (s.mSensorNumber.ToString() + " " + s.mTankNumber.ToString()) == ss.Value);
                        
                        someValueChanged(sender, e);
                    }
                }
            }

            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            someValueChanged(null, null);
            (Session["mSettings"] as TableGraphSettings).mType = ItemType.Table;
            Response.Redirect("ShowTable.aspx");
        }

        protected void ButtonShowGraph_Click(object sender, EventArgs e)
        {
            someValueChanged(null, null);
            (Session["mSettings"] as TableGraphSettings).mType = ItemType.Graph;
            Response.Redirect("ShowGraph.aspx");
        }
            
    }
}