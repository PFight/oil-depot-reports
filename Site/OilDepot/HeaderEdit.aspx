﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HeaderEdit.aspx.cs" Inherits="OilDepot.HeaderEdit" MasterPageFile="Site.Master" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
        <table>
            <tr>
                <td align="center" colspan="3">
                    <b>Текст заголовка:</b>                    
                </td>
            </tr>
            <tr valign="middle" >
                <td valign="top" colspan="3">
                    <asp:TextBox ID="TextBoxCaption" runat="server" Width="626px" 
                        Height="40px"></asp:TextBox>
                </td>
            </tr>
            <tr><td></td>
            </tr>
            <tr>
                
                <td >
                    <asp:Button ID="ButtonCancel" runat="server" Text="Отмена" Width="127px" 
                        onclick="ButtonCancel_Click" />
                </td>
                <td align="right">
                    <asp:Button ID="ButtonOk" runat="server" Text="Готово" Width="160px" 
                        onclick="ButtonOk_Click" />
                </td>
            </tr>            
        </table>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="HeadContent">
</asp:Content>
