﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportList.aspx.cs" Inherits="OilDepot.ReportList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table align="center" style="width: 428px">
        <tr>
            <td colspan=2>Сохраненные отчеты:</td>
        </tr>
        <tr>
            <td colspan="3"><asp:ListBox ID="ListBox1" Width="100%" runat="server" 
                    Height="246px"></asp:ListBox></td> 
            <td valign="top">&nbsp;</td>            
        </tr>
        <tr>
            <td align="center" >
                <asp:Button ID="ButtonAdd" runat="server" Text="Новый" 
                    onclick="ButtonAdd_Click" Width="94px" />
            </td>
            <td align="center" >
                <asp:Button ID="ButtonDelete" runat="server" Text="Удалить" 
                    onclick="ButtonDelete_Click" Width="90px" />
            </td>

            <td align="center" >
                <asp:Button ID="ButtonSelect" runat="server" Text="Открыть" 
                    onclick="ButtonSelect_Click" Width="92px" />
            </td>

        </tr>
    </table>
    <hr />
</asp:Content>
