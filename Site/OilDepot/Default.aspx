﻿<%@ Page Title="Домашняя страница" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="OilDepot._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Добро пожаловать в систему формирования отчетов о функционировании нефтебазы.
    </h2>
    <p>
        Вы можете просмотреть данные датчиков за некоторый период: <a href="GraphTableEdit.aspx" >просмотреть показания датчиков</a>
    </p>
    <p>
        Или же сформировать отчет, с возможностью экспортировать его в Word: <a href="ReportEdit.aspx" >cформировать отчет</a>
    </p>
</asp:Content>
