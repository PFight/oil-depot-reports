﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OilDepot
{
    public partial class TextEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mText"] == null)
            {
                TextReportItem set = new TextReportItem();
                setText(set);
                Session.Add("mText", set);
            }
            else if (!IsPostBack)
            {
                setText(Session["mText"] as TextReportItem);
            }
        }

        private void setText(TextReportItem cap)
        {
            TextBoxText.Text = cap.mText;
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            Session["mText"] = null;

            if (Session["PreviousPage"] != null)
            {
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void ButtonOk_Click(object sender, EventArgs e)
        {
            (Session["mText"] as TextReportItem).mText = TextBoxText.Text;

            if (Session["PreviousPage"] != null)
            {
                Session["DialogResult"] = true;
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }


    }
}