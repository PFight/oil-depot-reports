﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace OilDepot
{
    public partial class ShowTable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["mData"] == null)
            {
                TableGraphSettings mSettings = Session["mSettings"] as TableGraphSettings;
                if (mSettings != null)
                {
                    using (var con = DataBaseManager.OpenConnection())
                    {
                        TableGraphData mData = TableGraphData.loadFromDataBase(con, mSettings);
                        Session["mData"] = mData;
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                TableGraphSettings mSettings = Session["mSettings"] as TableGraphSettings;
                using(var con = DataBaseManager.OpenConnection())
                {
                    (Session["mData"] as TableGraphData).loadFromDataBase2(con, mSettings);
                }
            }

            //set gridView Datasource as dataTable dt.  
            GridView1.DataSource = initTable(Session["mData"] as TableGraphData);
            //Bind Datasource to gridview  
            GridView1.DataBind();
        }

        public DataTable initTable(TableGraphData data)
        {
            //instance of a datatable  
            DataTable dt = new DataTable();  
            //creating two datacolums Column1 and Column2   
            DataColumn dcol1 = new DataColumn("Время считывания", typeof(DateTime));
            dt.Columns.Add(dcol1);  

            foreach (SensorInfo sensor in data.getSettings().mSensors)
            {
                DataColumn dcol2 = new DataColumn(sensor.ToString(), typeof(double));
                dt.Columns.Add(dcol2);  
            }
              
            List<Readout> vals = new List<Readout>(data.getReadoutValues());
            while (vals.Count > 0)
            {
                // Magic to group readouts by ReadingTime
                DateTime time = vals[0].mReadingTime;
                List<Readout> timeReadouts = new List<Readout>(data.getSettings().mSensors.Count);
                foreach (Readout r in vals)
                {
                    if (r.mReadingTime == time)
                    {
                        timeReadouts.Add(r);
                    }
                }
                foreach (Readout r in timeReadouts)
                {
                    vals.Remove(r);
                }
                DataRow row = dt.NewRow();
                row[0] = time;
                for (int sensorIndx = 0; sensorIndx < data.getSettings().mSensors.Count; sensorIndx++)
                {
                    bool found = false;
                    foreach (Readout r in timeReadouts)
                    {
                        if (r.mSensor.Equals(data.getSettings().mSensors[sensorIndx]))
                        {
                            row[sensorIndx+1] = r.mReadoutValue;
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        row[sensorIndx + 1] = DBNull.Value;
                    }
                }
                dt.Rows.Add(row);
            }

            if (dt.Rows.Count == 0)
            {
                DataRow row = dt.NewRow();
                row[0] = "Нет данных";
                dt.Rows.Add(row);
            }

            return dt;
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("GraphTableEdit.aspx");
        }

        protected void ButtonExport_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            ExcelApp.Visible = false;
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
            ExcelWorkBook = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
            ExcelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(1);
            // Столбцы
            ExcelWorkSheet.Cells[1, 1] = "Время считывания";
            TableGraphData data = Session["mData"] as TableGraphData;
            for (int i = 0; i < data.getSettings().mSensors.Count; i++)
            {
                ExcelWorkSheet.Cells[1, i+2] = data.getSettings().mSensors[i].ToString();
            }
            // Строки
            DataTable dt = initTable(data);
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j] != null)
                    {
                        ExcelWorkSheet.Cells[i + 2, j + 1] = dt.Rows[i][j];
                    }
                }
            }
            string file = Request.MapPath("table.xlsx");
            if (File.Exists(file)) File.Delete(file);
            ExcelWorkBook.SaveAs(file);
            ExcelApp.Quit();
            System.Threading.Thread.Sleep(500);
            Response.Redirect("table.xlsx", true);
        }

        // Кнопка Готово
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["PreviousPage"] != null)
            {
                Session["DialogResult"] = true;
                Response.Redirect(Session["PreviousPage"].ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

    }
}