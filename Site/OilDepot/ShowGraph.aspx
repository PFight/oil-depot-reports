﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowGraph.aspx.cs" Inherits="OilDepot.ShowGraph" MasterPageFile="~/Site.Master" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    &nbsp;<table>
    <tr>
        <td>
            <asp:Button ID="ButtonBack" runat="server" Text="<< назад" 
                onclick="ButtonBack_Click" Width="124px" />
        </td>
         <td>
            <asp:Button ID="Button1" runat="server" Text="Готово" onclick="Button1_Click" 
                 Width="145px"  />
        </td>
    </tr>
    <tr>
        <td colspan=4>            
            
            <asp:Chart ID="Chart1" runat="server">
                <series>
                    <asp:Series Name="Series1">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </chartareas>
            </asp:Chart>
            
        </td>
    </tr>
</table>
</asp:Content>